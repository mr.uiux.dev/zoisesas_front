$(function () {
	// Tooltip
	const toolTip = function () {
		$(".view, .comment, .share").tooltip();
	};
	toolTip();
	if ($("[rel=tooltip]").length) {
		$("[rel=tooltip]").tooltip();
	}

	//SVG converter
	let svgImg = $("img.svg");
	const convertSVG = function () {
		svgImg.each(function () {
			var $img = jQuery(this);
			var imgID = $img.attr("id");
			var imgClass = $img.attr("class");
			var imgURL = $img.attr("src");
			jQuery.get(
				imgURL,
				function (data) {
					var $svg = jQuery(data).find("svg");
					if (typeof imgID !== "undefined") {
						$svg = $svg.attr("id", imgID);
					}
					if (typeof imgClass !== "undefined") {
						$svg = $svg.attr("class", imgClass + " replaced-svg");
					}
					$svg = $svg.removeAttr("xmlns:a");
					$img.replaceWith($svg);
				},
				"xml"
			);
		});
	};
	convertSVG();

	// Close Menu
	const closeMenu = $("#closeMenu");
	const navbarCollapse = $(".navbar-collapse");
	const btnMenu = $(".navbar-toggler");
	closeMenu.on("click", function () {
		navbarCollapse.removeClass("collapse show");
		btnMenu.attr("aria-expanded", "false");
	});

	// Main Slider
	const mainSlider = $(".main-slider");
	if (mainSlider.length > 0) {
		mainSlider.slick({
			arrows: true,
			dots: false,
			autoplay: true,
			autoplaySpeed: 3000,
		});
		mainSlider.on("beforeChange", function (event, slick) {
			convertSVG();
			toolTip();
		});
	}

	// Daterange picker
	const start = new Date();
	const end = new Date(new Date().setYear(start.getFullYear() + 1));

	$("#fromDate")
		.datepicker({
			startDate: start,
			endDate: end,
		})
		.on("changeDate", function () {
			$("#toDate").datepicker("setStartDate", new Date($(this).val()));
		});

	$("#toDate")
		.datepicker({
			startDate: start,
			endDate: end,
		})
		.on("changeDate", function () {
			$("#fromDate").datepicker("setEndDate", new Date($(this).val()));
		});

	// Recent Articles Slider
	const articlesSlider = $(".articles-slider");
	if (articlesSlider.length > 0) {
		articlesSlider.slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			vertical: true,
			rows: 3,
			arrows: true,
			dots: false,
		});
		articlesSlider.on("beforeChange", function (event, slick) {
			convertSVG();
			toolTip();
		});
	}

	// Recent Notices Slider
	const noticesSlider = $(".notices-slider");
	if (noticesSlider.length > 0) {
		noticesSlider.slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			vertical: true,
			rows: 3,
			arrows: true,
			dots: false,
		});
		noticesSlider.on("beforeChange", function (event, slick) {
			convertSVG();
			toolTip();
		});
	}

	// Recent Tributes Slider
	const tributesSlider = $(".tributes-slider");
	if (tributesSlider.length > 0) {
		tributesSlider.slick({
			slidesToShow: 1,
			slidesToScroll: 3,
			vertical: true,
			rows: 5,
			arrows: true,
			dots: false,
			responsive: [
				{
					breakpoint: 769,
					settings: {
						slidesToShow: 2,
						slidesToScroll: 3,
						rows: 3,
						vertical: false,
					},
				},
				{
					breakpoint: 480,
					settings: {
						slidesToShow: 1,
						slidesToScroll: 1,
					},
				},
			],
		});
		tributesSlider.on("beforeChange", function (event, slick) {
			convertSVG();
			toolTip();
		});
	}

	// Latest Tributes Slider
	const latestTributesSlider = $(".latest-tributes-slider");
	if (latestTributesSlider.length > 0) {
		latestTributesSlider.slick({
			slidesToShow: 3,
			slidesToScroll: 1,
			arrows: true,
			dots: false,
			responsive: [
				{
					breakpoint: 1024,
					settings: {
						slidesToShow: 3,
						slidesToScroll: 1,
					},
				},
				{
					breakpoint: 991,
					settings: {
						slidesToShow: 2,
						slidesToScroll: 1,
					},
				},
				{
					breakpoint: 768,
					settings: {
						slidesToShow: 1,
						slidesToScroll: 1,
					},
				},
			],
		});
		latestTributesSlider.on("beforeChange", function (event, slick) {
			convertSVG();
			toolTip();
		});
	}

	// Login Slider
	const loginSlider = $(".login-slider");
	if (loginSlider.length > 0) {
		loginSlider.slick({
			arrows: false,
			dots: false,
			autoplay: true,
			autoplaySpeed: 2000,
			fade: true,
			cssEase: "linear",
		});
	}

	// Sticky sidebar
	const fixedSidebar = $(".sticky-sidebar");
	if (fixedSidebar.length > 0) {
		fixedSidebar.theiaStickySidebar();
	}

	// Intl Phone
	const phone = $('[id*="phone-"]');
	if (phone.length > 0) {
		phone.intlTelInput({
			allowExtensions: true,
			formatOnDisplay: true,
			autoFormat: true,
			autoHideDialCode: true,
			autoPlaceholder: true,
			autoHideDialCode: true,
			defaultCountry: "auto",
			ipinfoToken: "yolo",
			nationalMode: true,
			numberType: "MOBILE",
			preferredCountries: ["gr", "sa", "ae", "qa", "om", "bh", "kw", "ma"],
			preventInvalidNumbers: true,
			separateDialCode: false,
			initialCountry: "gr",
		});
	}

	// Notice Slider
	const noticeSlider = $(".notice-slider");
	const noticeSliderNav = $(".notice-slider-nav");
	if (noticeSlider.length > 0) {
		noticeSlider.slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			arrows: false,
			dots: false,
			fade: true,
			asNavFor: ".notice-slider-nav",
		});
	}
	if (noticeSliderNav.length > 0) {
		noticeSliderNav.slick({
			slidesToShow: 5,
			slidesToScroll: 1,
			asNavFor: ".notice-slider",
			dots: false,
			arrows: true,
			focusOnSelect: true,
		});
	}

	// Open Gallery Notice
	const openGallery = $(".view-gallery");
	const noticeGallery = $(".notice-gallery");
	if (noticeGallery.length > 0) {
		lightbox.option({
			resizeDuration: 200,
			wrapAround: true,
		});
	}
	openGallery.click(function () {
		$(".notice-gallery a:first").click();
	});

	// Candle Slider
	const candleSlider = $(".candle-slider");
	if (candleSlider.length > 0) {
		candleSlider.slick({
			slidesToShow: 4,
			slidesToScroll: 1,
			arrows: true,
			dots: false,
			centerMode: true,
			focusOnSelect: true,
		});
	}
	$(".modal").on("shown.bs.modal", function (e) {
		$('a[data-toggle="tab"]').on("shown.bs.tab", function (e) {
			candleSlider.slick("setPosition");
		});
	});

	// Sweetalert
	const successAlert = $('[id*="success-"]');
	if (successAlert.length > 0) {
		$(document).on("click", '[id*="success-"]', function (e) {
			swal({
				title: "Congratulations",
				text: "Your account is upgraded to enjoy our features!",
				type: "success",
				showConfirmButton: true,
				confirmButtonText: "Go to Dashboard",
			}).then(function () {
				window.location = "dashboard-profile.html";
			});
		});
	}
	// Mansory
	const gridMasonry = $(".grid");
	if (gridMasonry.length > 0) {
		$(window).on("load", function () {
			gridMasonry.masonry({
				itemSelector: ".grid-item", // use a separate class for itemSelector, other than .col-
				columnWidth: ".grid-sizer",
				percentPosition: true,
			});
		});
	}
});
